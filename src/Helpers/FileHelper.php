<?php
namespace Xsoft\FileManager;

class FileHelper
{

    public static function getTypesExtensions($types)
    {
        $extensions = [];
        $configTypes = config('filemanager.types');
        foreach ($types as $type) {
            if (key_exists($type, $configTypes)) {
                $extensions = array_merge($extensions, $configTypes[$type]['extensions']);
            }
        }
        $result = '';
        foreach ($extensions as $key => $extension) {
            if ($key > 0) {
                $result = $result . ',.' . $extension;
            } else {
                $result = '.' . $extension;
            }

        }
        return $result;
    }
}
