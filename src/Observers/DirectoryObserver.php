<?php

namespace Xsoft\FileManager;

use Illuminate\Support\Facades\Storage;

class DirectoryObserver
{


    public function deleted(Directory $directory)
    {
        Storage::deleteDirectory($directory->storage_path);
        foreach ($directory->directories as $dir){
            $dir->delete();
        }
        foreach($directory->files as $file){
            $file->delete();
        }
    }

}
