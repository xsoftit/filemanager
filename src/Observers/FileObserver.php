<?php

namespace Xsoft\FileManager;

use Illuminate\Support\Facades\Storage;

class FileObserver
{


    public function deleted(File $file)
    {
        Storage::delete($file->storage_path);
    }

}
