<?php

namespace Xsoft\FileManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Directory extends Model
{
    protected $fillable = [
        'name',
        'storage_path',
        'parent_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


    public function parent()
    {
        return $this->belongsTo(Directory::class, 'parent_id');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'parent_id');
    }

    public function directories()
    {
        return $this->hasMany(Directory::class, 'parent_id')->orderBy('name');
    }

    public function getIdAttribute($value)
    {
        if (!$value) {
            $value = 0;
        }
        return $value;
    }

    public function getPathAttribute()
    {
        return Storage::url($this->storage_path);
    }

    public function getRouteAttribute()
    {
        return route('files.getFiles', ['directory' => $this->id]);
    }

    public function getTypesAttribute()
    {
        $types = [];
        foreach ($this->files as $file){
            if(!in_array($file->type,$types)){
                $types[] = $file->type;
            }
        }
        return $types;
    }

    public function getMarginAttribute()
    {
        if ($this->parent_id) {
            if ($this->parent_id == 'main') {
                $margin = 0;
            } else {
                $margin = $this->parent->margin + 15;
            }
        } else {
            $margin = 15;
        }
        return $margin;
    }

    public function checkTypes($types){
        if(!$types){
            if($this->files->count()){
                return true;
            }
        }else {
            foreach ($types as $type) {
                if (in_array($type, $this->types)) {
                    return true;
                }
            }
        }
        return false;
    }
}
