<?php

namespace Xsoft\FileManager;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $fillable = [
        'name',
        'alt',
        'storage_name',
        'storage_path',
        'extension',
        'parent_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $types;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->types = config('filemanager.types');
    }


    public function parent()
    {
        return $this->belongsTo(Directory::class, 'parent_id');
    }

    public function getPathAttribute()
    {
        return Storage::url($this->storage_path);
    }

    public function getAltAttribute($value){
        if(!$value){
            $value = $this->name;
        }
        return $value;
    }

    public function getTypeAttribute(){
        $type = 'other';
        foreach ($this->types as $type_name => $type_value){
            foreach($type_value['extensions'] as $extension){
                if($this->extension == $extension){
                    $type = $type_name;
                }
            }
        }
        return $type;
    }

    public function getPreviewAttribute(){
        $preview = 'other';
        if(key_exists($this->type,$this->types)){
            if(key_exists('preview',$this->types[$this->type])){
                $preview = $this->types[$this->type]['preview'];
            }
        }
        return $preview;
    }

    public function getDetailsAttribute(){
        $details = [];
        $details[__('filemanager.details.name')] = $this->name;
        $details[__('filemanager.details.alt')] = $this->alt;
        $details[__('filemanager.details.path')] = $this->path;
        $details[__('filemanager.details.type')] = $this->type;
        $size = $this->getSize();
        $details[__('filemanager.details.size')] = $size;
        $lastModified = Storage::lastModified($this->storage_path);
        $details[__('filemanager.details.lastModified')] = Carbon::createFromTimestamp($lastModified)->format('d.m.Y');
        return $details;
    }


    public function checkTypes($types){
        foreach ($types as $type){
            if($type == $this->type){
                return true;
            }
        }
        return false;
    }

    private function getSize(){
        $size = Storage::size($this->storage_path);
        $sufix = 'B';
        if($size > 1000000){
            $size = round($size/1000000,2);
            $sufix = 'MB';
        }
        else if($size > 1000){
            $size = round($size/1000,2);
            $sufix = 'kB';
        }
        return $size.$sufix;
    }
}
