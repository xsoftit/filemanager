<?php

namespace Xsoft\FileManager;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile as NewFile;

class FilesImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import files from storage directory to datatbase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::allfiles('public/files');
        foreach ($files as $filePath) {
            if (!strpos($filePath, 'gitignore')) {
                if (!File::where('storage_path', $filePath)->first()) {
                    $shortFilePath = str_replace('public/files/', '', $filePath);
                    $explodedFilePath = explode('/', $shortFilePath);
                    $parent_id = null;
                    foreach ($explodedFilePath as $key => $name) {
                        if($name == 'temp'){
                            break;
                        }
                        if ($key == (count($explodedFilePath) - 1)) {
                            File::create([
                                'parent_id' => $parent_id,
                                'name' => $name,
                                'storage_path' => $filePath,
                                'type' => File::checkType(new NewFile(Storage::path($filePath),$name))
                            ]);
                            echo $filePath.' saved'.PHP_EOL;
                        } else {
                            $directory = File::where('name', $name)->where('type', 'directory')->where('parent_id',$parent_id)->first();
                            if (!$directory) {
                                $directory = File::create([
                                    'parent_id' => $parent_id,
                                    'name' => $name,
                                    'storage_path' => null,
                                    'type' => 'directory'
                                ]);
                            }
                            $parent_id = $directory->id;
                        }
                    }
                }
            }
        }
        echo 'SUCCESS!'.PHP_EOL;
    }
}
