<?php

namespace Xsoft\FileManager;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class FilesClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'files:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear not used files from storage directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::allfiles('public/files');
        foreach ($files as $filePath) {
            if (!strpos($filePath, 'gitignore')) {
                if (!File::where('storage_path', $filePath)->first()) {
                    Storage::delete($filePath);
                    echo $filePath.' deleted'.PHP_EOL;
                }
            }
        }
        echo 'SUCCESS!'.PHP_EOL;
    }
}
