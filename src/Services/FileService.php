<?php

namespace Xsoft\FileManager;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileService
{

    public static function prepareMainDirectoryObject(){
        return Directory::make([
            'id' => 0,
            'parent_id' => 'main',
            'name' => '/',
            'storage_path' => 'public'
        ]);
    }

    public static function getDirectoriesView($directory, $types)
    {
        $mainDirectory = self::prepareMainDirectoryObject();
        $directories = Directory::where('parent_id', 0)->orderBy('name')->get();
        return view('fileManager::partials.directories', [
            'currentDirectory' => $directory,
            'mainDirectory' => $mainDirectory,
            'directories' => $directories,
            'types' => json_decode($types, true)
        ])->render();
    }

    public static function getFilesView($directory, $types)
    {
        $files = File::where('parent_id', $directory)->get();
        if ($types) {
            $files = $files->filter(function ($value) use ($types) {
                return $value->checkTypes(json_decode($types, true));
            });
        }
        return view('fileManager::partials.files', [
            'files' => $files,
        ])->render();
    }


    public static function moveDirectory($directory, $target)
    {
        $path = $target->storage_path . '/'.$directory->name;
        Storage::move($directory->storage_path, $path);
        self::moveDirectoryContent($directory,$target);
    }

    private static function moveDirectoryContent($directory,$target){
        $path = $target->storage_path . '/'.$directory->name;
        $directory->update([
            'parent_id' => $target->id,
            'storage_path' => $path
        ]);
        foreach ($directory->files as $file){
            $file->update([
                'storage_path' => $path. '/'.$file->storage_name
            ]);
        }
        foreach($directory->directories as $dir){
            self::moveDirectoryContent($dir,$directory);
        }
    }

    public static function moveFile($file, $target)
    {
        $path = $target->storage_path . '/'.$file->storage_name;
        Storage::move($file->storage_path, $path);
        $file->update([
            'parent_id' => $target->id,
            'storage_path' => $path
        ]);
    }


    public static function createDirectory($name, $parent_id = 0)
    {
        $path = 'public';
        if ($parent_id) {
            $parent = Directory::find($parent_id);
            $path = $parent->storage_path;
        }
        $storage_path = $path . '/' . $name;
        Storage::makeDirectory($storage_path);
        return Directory::create([
            'name' => $name,
            'parent_id' => $parent_id,
            'storage_path' => $storage_path
        ]);
    }

    public static function createFile($file, $parent_id = 0)
    {
        $extension = Str::lower($file->getClientOriginalExtension());
        $name = $file->getClientOriginalName();
        $filename = uniqid() . '.' . $extension;
        $path = 'public';
        if ($parent_id) {
            $parent = Directory::find($parent_id);
            $path = $parent->storage_path;
        }
        $storage_path = Storage::putFileAs($path, $file, $filename);
        return File::create([
            'name' => $name,
            'parent_id' => $parent_id,
            'storage_name' => $filename,
            'storage_path' => $storage_path,
            'extension' => $extension
        ]);
    }
}
