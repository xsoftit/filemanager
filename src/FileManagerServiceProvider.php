<?php

namespace Xsoft\FileManager;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class FileManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            FilesImport::class,
            FilesClear::class,
        ]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'fileManager');

        // Observers
        File::observe(FileObserver::class);
        Directory::observe(DirectoryObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('FileHelper', FileHelper::class);
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            $bladeCompiler->directive('fileManager', function ($options) {
                return "<?php echo Xsoft\\AdminPanel\\DeleteButton::draw($options); ?>";
            });

        });
    }
}
