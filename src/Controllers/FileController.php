<?php

namespace Xsoft\FileManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FileController extends Controller
{
    protected $types;

    public function __construct()
    {
        $this->types = config('filemanager.types');
    }

    public function getDirectories($directory, $types = '')
    {
        $html = FileService::getDirectoriesView($directory, $types);
        return response()->json(['html' => $html]);
    }

    public function getFiles($directory, $types = '')
    {
        $html = FileService::getFilesView($directory, $types);
        return response()->json(['html' => $html]);
    }

    public function getFileDetails(Request $request)
    {
        $file = File::find($request->get('id'));
        $html = view('fileManager::partials.items.fileDetails', [
            'file' => $file,
        ])->render();
        return response()->json(['html' => $html]);
    }

    public function getFileEditView(Request $request)
    {
        $file = File::find($request->get('id'));
        $html = view('fileManager::partials.items.fileEdit', [
            'file' => $file,
        ])->render();
        return response()->json(['html' => $html, 'type' => $file->type]);
    }

    public function edit(Request $request)
    {
        $file = File::find($request->get('id'));
        if ($file->type == 'image') {
            $crop = $request->get('crop');
            $img = Image::make(public_path($file->path));
            $img->crop(intval($crop['width']), intval($crop['height']), intval($crop['x']), intval($crop['y']));
            $img->save(public_path($file->path));
        }
        $file->update([
            'name' => $request->get('name'),
            'alt' => $request->get('alt')
        ]);
        $html = FileService::getFilesView($request->get('current'), $request->get('types'));
        return response()->json(['html' => $html]);
    }

    public function moveDirectory(Request $request)
    {
        $directory = Directory::find($request->get('item'));
        $target = Directory::find($request->get('target'));
        if (!$target) {
            $target = FileService::prepareMainDirectoryObject();
        }
        if ($directory->parent_id != $target->id) {
            FileService::moveDirectory($directory, $target);
        }
        $directoriesHtml = FileService::getDirectoriesView($request->get('current'), $request->get('types'));
        $filesHtml = FileService::getFilesView($request->get('current'), $request->get('types'));
        return response()->json(['directoriesHtml' => $directoriesHtml, 'filesHtml' => $filesHtml]);
    }


    public function moveFile(Request $request)
    {
        $files = File::find($request->get('item'));
        $target = Directory::find($request->get('target'));
        if (!$target) {
            $target = FileService::prepareMainDirectoryObject();
        }
        foreach ($files as $file) {
            if ($file->parent_id != $target->id) {
                FileService::moveFile($file, $target);
            }
        }
        $directoriesHtml = FileService::getDirectoriesView($request->get('current'), $request->get('types'));
        $filesHtml = FileService::getFilesView($request->get('current'), $request->get('types'));
        return response()->json(['directoriesHtml' => $directoriesHtml, 'filesHtml' => $filesHtml]);
    }

    public function createDirectory(Request $request)
    {
        FileService::createDirectory($request->get('name'), $request->get('parent_id'));
        $html = FileService::getDirectoriesView($request->get('parent_id'), $request->get('types'));
        return response()->json(['html' => $html]);
    }

    public function addFiles(Request $request)
    {
        $parent_id = $request->get('parent_id');
        $files = $request->File('files');
        if ($files) {
            foreach ($files as $file) {
                FileService::createFile($file, $parent_id);
            }
        }
        $html = FileService::getFilesView($parent_id, $request->get('types'));
        return response()->json(['html' => $html]);
    }

    public function deleteDirectory(Request $request)
    {
        $directory = Directory::find($request->get('id'));
        $directory->delete();
        $directoriesHtml = FileService::getDirectoriesView($request->get('current'), $request->get('types'));
        $filesHtml = FileService::getFilesView($request->get('current'), $request->get('types'));
        return response()->json(['directoriesHtml' => $directoriesHtml, 'filesHtml' => $filesHtml]);

    }

    public function deleteFile(Request $request)
    {
        $file = File::find($request->get('id'));
        $file->delete();
        $directoriesHtml = FileService::getDirectoriesView($request->get('current'), $request->get('types'));
        $filesHtml = FileService::getFilesView($request->get('current'), $request->get('types'));
        return response()->json(['directoriesHtml' => $directoriesHtml, 'filesHtml' => $filesHtml]);
    }

    public function downloadFile($id)
    {
        $file = File::find($id);
        return Storage::download($file->storage_path, $file->name);
    }
}
