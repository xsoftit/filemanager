<?php

return [
    /**
     * Definition of file types. Each type must contain keys 'preview' and 'extensions'
     * 'extensions' defines the file extensions of a given type
     * 'preview' defines how the files will be displayed
     * Available previews: ['image','video','pdf','other']
     *
     * Files with extensions not defined here, will be have type 'other'
     **/
    'types' => [
        'image' => [
            'preview' => 'image',
            'extensions' => ['png', 'jpg', 'jpeg', 'gif'],
        ],
        'icon' => [
            'preview' => 'image',
            'extensions' => ['svg'],
        ],
        'video' => [
            'preview' => 'video',
            'extensions' => ['mp4', 'avi', 'wmv'],
        ],
        'pdf' => [
            'preview' => 'pdf',
            'extensions' => ['pdf'],
        ],
    ]
];
