<?php


Route::name('files.')->prefix('files')->middleware('web')->group(function () {
    Route::get('/getDirectories/{directory}/{types?}', 'Xsoft\FileManager\FileController@getDirectories')->name('getDirectories');
    Route::get('/getFiles/{directory}/{types?}', 'Xsoft\FileManager\FileController@getFiles')->name('getFiles');
    Route::post('/getFileDetails', 'Xsoft\FileManager\FileController@getFileDetails')->name('getFileDetails');
    Route::post('/getFileEditView', 'Xsoft\FileManager\FileController@getFileEditView')->name('getFileEditView');
    Route::post('/edit', 'Xsoft\FileManager\FileController@edit')->name('edit');
    Route::post('/moveDirectory', 'Xsoft\FileManager\FileController@moveDirectory')->name('moveDirectory');
    Route::post('/moveFile', 'Xsoft\FileManager\FileController@moveFile')->name('moveFile');
    Route::post('/createDirectory', 'Xsoft\FileManager\FileController@createDirectory')->name('createDirectory');
    Route::post('/addFiles', 'Xsoft\FileManager\FileController@addFiles')->name('addFiles');
    Route::post('/deleteDirectory', 'Xsoft\FileManager\FileController@deleteDirectory')->name('deleteDirectory');
    Route::post('/deleteFile', 'Xsoft\FileManager\FileController@deleteFile')->name('deleteFile');
    Route::get('/downloadFile/{id}', 'Xsoft\FileManager\FileController@downloadFile')->name('downloadFile');
});

