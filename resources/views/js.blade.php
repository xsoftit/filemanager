@section('js')
    @parent
    <script>
        //Variables

        var body = $('body');
        var filemanagerWindow = $('.filemanager-window');
        var filemanagerShadow = $('.filemanager-background-shadow');
        var filemanagerOpen = $('.filemanager-open');
        var directoriesContainer = filemanagerWindow.find('.filemanager-directories');
        var filesContainer = filemanagerWindow.find('.filemanager-files');
        var cropper;
        var loaderTimeout = null;
        var routes = {
            getFileDetails: '{{route("files.getFileDetails")}}',
            getFileEditView: '{{route("files.getFileEditView")}}',
            edit: '{{route("files.edit")}}',
            createDirectory: '{{route("files.createDirectory")}}',
            addFiles: '{{route('files.addFiles')}}',
            moveFile: '{{route("files.moveFile")}}',
            moveDirectory: '{{route("files.moveDirectory")}}',
            deleteDirectory: '{{route('files.deleteDirectory')}}',
            deleteFile: '{{route("files.deleteFile")}}',
            downloadFile: "{{route('files.downloadFile',['id' => 'PLACEHOLDER'])}}",
        };
        var translations = {
          directory: '{{__("filemanager.directory")}}',
          file: '{{__("filemanager.file")}}',
        };

        //Events

        body.on('fileManager:open', function () {
            filemanagerWindow.show(function () {
                $(this).css('opacity', '1');
                $(this).css('margin-top', '0');
            });
            filemanagerShadow.show(function () {
                $(this).css('opacity', '1');
            });
            let directoriesRoute = filemanagerWindow.attr('data-directories-route');
            let filesRoute = filemanagerWindow.attr('data-files-route');
            if (filemanagerWindow.attr('data-types')) {
                directoriesRoute = directoriesRoute + '/' + filemanagerWindow.attr('data-types');
                filesRoute = filesRoute + '/' + filemanagerWindow.attr('data-types');
            }
            getContent(directoriesRoute, directoriesContainer, function () {
                initDirectoriesMoving();
            });
            getContent(filesRoute, filesContainer, function () {
                initFilesMoving();
            });

        });

        body.on('fileManager:close', function () {
            filemanagerWindow.hide(300, function () {
                $(this).css('margin-top', '-100px');
                $(this).css('opacity', '0');
                filemanagerWindow.find('.filemanager-upload-box').css('display', 'none');
                filemanagerWindow.find('.file-upload-input').val('').trigger('change');
            });
            filemanagerShadow.hide(300, function () {
                $(this).css('opacity', '0');
            });
        });

        $(document).mousedown(function () {
            mouseDown = true;
        }).mouseup(function () {
            mouseDown = false;
        });

        //FileManager Display

        filemanagerOpen.on('click', function () {
            $.each(this.attributes, function () {
                if (this.specified && this.name.includes('data-')) {
                    filemanagerWindow.attr(this.name, this.value);
                }
            });
            body.trigger('fileManager:open')
        });

        body.on('click', '.filemanager-background-shadow', function () {
            body.trigger('fileManager:close')
        });

        body.on('click', '.filemanager-close', function () {
            body.trigger('fileManager:close')
        });

        filemanagerWindow.on('click', '.directory', function (e) {
            if ($(e.target).hasClass('directory-delete')) {
                return false;
            }
            ;
            filemanagerWindow.find('.new-directory-box').remove();
            let directory = $(this);
            let filesRoute = directory.attr('data-route');
            if (filemanagerWindow.attr('data-types')) {
                filesRoute = filesRoute + '/' + filemanagerWindow.attr('data-types');
            }
            getContent(filesRoute, filesContainer, function () {
                directoriesContainer.find('.directory').removeClass('active');
                directory.addClass('active');
                initFilesMoving();
            });
        });

        function addLoader() {
            loaderTimeout = setTimeout(function () {
                let item = filemanagerWindow.find('.filemanager-body');
                item.addClass('filemanager-loading');
                item.append('<span class="filemanager-loader"></span>');
            }, 300);
        }

        function removeLoader() {
            clearTimeout(loaderTimeout);
            setTimeout(function () {
                let item = filemanagerWindow.find('.filemanager-body');
                item.removeClass('filemanager-loading');
                item.find('.filemanager-loader').remove();
            }, 301);
        }

        function getContent(route, container, callback) {
            addLoader();
            $.ajax({
                method: "GET",
                url: route,
                success: function (response) {
                    container.html(response.html ? response.html : '');
                    removeLoader();
                    if (callback) {
                        callback();
                    }
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        }

        //Sub Windows
        filemanagerWindow.on('click', '.filemanager-sub-window-close', function () {
            let parent = $(this).parents('.filemanager-sub-window');
            parent.css('display', 'none');
            parent.find('input').val('').trigger('change');
        });

        //File Download
        filemanagerWindow.on('click', '.file-download', function () {
            let item = $(this);
            let route = routes.downloadFile;
            route = route.replace('PLACEHOLDER', item.attr('data-id'));
            window.location = route;
        });

        //Directory create

        filemanagerWindow.on('click', '.new-directory', function () {
            filemanagerWindow.find('.new-directory-box').remove();
            let activeDirectory = filemanagerWindow.find('.directory.active');
            let html = '<div class="new-directory-box"  style="margin-left: ' + activeDirectory.css("margin-left") + '">' +
                '<input data-id="' + activeDirectory.attr('data-id') + '" class="directory-name" style="margin-left: 15px">' +
                '<span class="create-directory filemanager-button filemanager-button-small filemanager-button-primary"><i class="mdi mdi-check"></i></span>' +
                '</div>';
            activeDirectory.after(html);
        });

        filemanagerWindow.on('click', '.create-directory', function () {
            let box = $(this).parents('.new-directory-box');
            createDirectory(box);
        });

        function createDirectory(box) {
            let data = {
                name: box.find('input').val(),
                parent_id: box.find('input').attr('data-id')
            };
            if (filemanagerWindow.attr('data-types')) {
                data['types'] = filemanagerWindow.attr('data-types');
            }
            addLoader();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                url: routes.createDirectory,
                data: data,
                dataType: 'json',
                success: function (response) {
                    directoriesContainer.html(response.html ? response.html : '');
                    removeLoader();
                    initDirectoriesMoving();
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        }

        //File Upload

        filemanagerWindow.on('click', '.new-files', function () {
            let uploadWindow = filemanagerWindow.find('.filemanager-upload-window');
            if (filemanagerWindow.attr('data-extensions')) {
                uploadWindow.find('.file-upload-input').attr('accept', filemanagerWindow.attr('data-extensions'));
            }
            uploadWindow.css('display', 'flex');
        });

        filemanagerWindow.on('change', '.file-upload-input', function () {
            let input = $(this);
            let html = '';
            if (input.prop('files').length > 0) {
                input.siblings('.filemanager-upload-info').hide();
                $.each(event.target.files, function (index, file) {
                    html += '<span>' + file.name + '</span>';
                });
                input.siblings('.filemanager-uploaded').html(html);
            } else {
                input.siblings('.filemanager-upload-info').show();
                input.siblings('.filemanager-uploaded').html('');
            }
        });

        filemanagerWindow.on('click', '.upload-files', function () {
            addFiles();
        });

        function addFiles() {
            let parent_id = filemanagerWindow.find('.directory.active').attr('data-id');
            let files = filemanagerWindow.find('.file-upload-input').prop('files');
            let filesData = new FormData();
            $.each(files, function (index, file) {
                filesData.append('files[' + index + ']', file);
            });
            addLoader();
            filesData.append('parent_id', parent_id);
            if (filemanagerWindow.attr('data-types')) {
                filesData.append('types', filemanagerWindow.attr('data-types'));
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: filesData,
                enctype: 'multipart/form-data',
                cache: false,
                contentType: false,
                processData: false,
                type: "POST",
                dataType: 'json',
                url: routes.addFiles,
                success: function (response) {
                    filesContainer.html(response.html ? response.html : '');
                    filemanagerWindow.find('.filemanager-upload-box').css('display', 'none');
                    filemanagerWindow.find('.file-upload-input').val('').trigger('change');
                    removeLoader();
                    initFilesMoving();
                    filemanagerWindow.find('.filemanager-upload-window').hide();
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        }

        //File Info
        filemanagerWindow.on('click', '.file-info', function () {
            let infoWindow = filemanagerWindow.find('.filemanager-file-info-window');
            let id = $(this).attr('data-id');
            infoWindow.find('.file-edit').attr('data-id',id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    id: id
                },
                type: "POST",
                dataType: 'json',
                url: routes.getFileDetails,
                success: function (response) {
                    infoWindow.find('.filemanager-body').html(response.html);
                    infoWindow.css('display', 'flex');
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        });

        //File Edit

        filemanagerWindow.on('click', '.file-edit', function () {
            filemanagerWindow.find('.filemanager-file-info-window').hide();
            let editWindow = filemanagerWindow.find('.filemanager-file-edit-window');
            let id = $(this).attr('data-id');
            editWindow.find('.submit-file-edit').attr('data-id',id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: {
                    id: id
                },
                type: "POST",
                dataType: 'json',
                url: routes.getFileEditView,
                success: function (response) {
                    editWindow.find('.filemanager-body').html(response.html);
                    editWindow.css('display', 'flex');
                    if(response.type == 'image') {
                        let img = document.getElementById('img-edit');
                        cropper = new Cropper(img,{
                            autoCropArea: 1
                        });
                    }else{
                        cropper = null;
                    }
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        });

        filemanagerWindow.on('click','.submit-file-edit',function(){
            let editWindow = filemanagerWindow.find('.filemanager-file-edit-window');
            let id = $(this).attr('data-id');
            let data = {
                id: id,
                name: editWindow.find('input[name="name"]').val(),
                alt:  editWindow.find('input[name="alt"]').val(),
                current: filemanagerWindow.find('.directory.active').attr('data-id')
            };
            if (cropper) {
                console.log(cropper.getData());
                data['crop'] = cropper.getData();
            }
            if (filemanagerWindow.attr('data-types')) {
                data['types'] = filemanagerWindow.attr('data-types');
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                data: data,
                type: "POST",
                dataType: 'json',
                url: routes.edit,
                success: function (response) {
                    cropper = null;
                    filesContainer.html(response.html ? response.html : '');
                    editWindow.hide();
                    filemanagerWindow.find('.file-info[data-id="'+id+'"]').click();
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        });

        //Files/Directory Delete

        filemanagerWindow.on('click', '.directory-delete', function () {
            let directory = $(this).parent();
            let deleteWindow = filemanagerWindow.find('.filemanager-delete-window');
            deleteWindow.find('.delete-object').html(translations.directory+' '+directory.text());
            deleteWindow.find('.delete-id').val(directory.attr('data-id'));
            deleteWindow.find('.submit-delete').attr('data-route',routes.deleteDirectory);
            filemanagerWindow.find('.filemanager-delete-window').css('display', 'flex');
        });


        filemanagerWindow.on('click', '.file-delete', function () {
            let file = $(this).parents('.file-box');
            let deleteWindow = filemanagerWindow.find('.filemanager-delete-window');
            deleteWindow.find('.delete-object').html(translations.file+' '+file.find('.file-name').text());
            deleteWindow.find('.delete-id').val(file.attr('data-id'));
            deleteWindow.find('.submit-delete').attr('data-route',routes.deleteFile);
            filemanagerWindow.find('.filemanager-delete-window').css('display', 'flex');
        });

        filemanagerWindow.on('click', '.submit-delete', function () {
            let deleteWindow = filemanagerWindow.find('.filemanager-delete-window');
            let route = $(this).attr('data-route');
            let id =  deleteWindow.find('.delete-id').val();
            let data = {
                id: id,
                current: filemanagerWindow.find('.directory.active').attr('data-id')
            };
            if (filemanagerWindow.attr('data-types')) {
                data['types'] = filemanagerWindow.attr('data-types');
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                url: route,
                data: data,
                dataType: 'json',
                success: function (response) {
                    deleteWindow.hide();
                    directoriesContainer.html(response.directoriesHtml ? response.directoriesHtml : '');
                    filesContainer.html(response.filesHtml ? response.filesHtml : '');
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        });

        //Files/Directory Move

        filemanagerWindow.on('click', '.file-box', function (e) {
            let target = $(e.target);
            if(!target.hasClass('filemanager-button') && !target.is('i')) {
                $(this).toggleClass('selected');
            }
        });

        filemanagerWindow.on('click', function (e) {
            let v = $(e.target);
            if (!v.hasClass('selected') && !v.parents('.file-box').length) {
                filemanagerWindow.find('.file-box.selected').removeClass('selected');
            }
        });

        function initDirectoriesMoving() {
            let directories = body.find('.directory');
            directories.not('[data-id="0"]').draggable({
                addClasses: false,
                delay: 200,
                snapTolerance: 300,
                revert: "invalid",
                helper: "clone",
                axis: 'y',
                start: function (event, ui) {
                    let item = $(this);
                    item.hide();
                    findDirectoryChildren(item, 'hide');
                },
                stop: function (event, ui) {
                    let item = $(this);
                    item.show();
                    findDirectoryChildren(item, 'show');
                }
            });
            directories.droppable({
                classes: {
                    "ui-droppable-hover": "hover"
                },
                tolerance: "pointer",
                drop: function (event, ui) {
                    let route;
                    let target = $(this);
                    let item = ui.draggable;
                    if (item.hasClass('directory')) {
                        route = routes.moveDirectory;
                        item = item.attr('data-id');
                    } else {
                        route = routes.moveFile;
                        item = [item.attr('data-id')];
                        if (filemanagerWindow.find('.file-box.selected').length > 0) {
                            item = filemanagerWindow.find(".file-box.selected").map(function () {
                                return $(this).attr("data-id");
                            }).get()
                        }
                    }
                    move(target.attr('data-id'), item, route);
                }
            });
        }

        function initFilesMoving() {
            let files = body.find('.file-box');
            let objects, originalObjects;
            files.draggable({
                addClasses: false,
                delay: 200,
                snapTolerance: 300,
                helper: "clone",
                revert: "invalid",
                classes: {
                    "ui-draggable-dragging": "mini"
                },
                start: function (event, ui) {
                    let item = $(this);
                    let helper = ui.helper;
                    item.css('opacity', 0.5);
                    if (!helper.hasClass('selected')) {
                        filemanagerWindow.find('.file-box.selected').removeClass('selected');
                    } else {
                        originalObjects = filemanagerWindow.find('.file-box.selected[data-id!="' + item.attr('data-id') + '"]');
                        originalObjects.css('opacity', 0.5)
                        objects = originalObjects.clone();
                        $(this).after(objects);
                        objects.css('position', 'absolute');
                        objects.addClass('mini');
                    }
                },
                drag: function (event, ui) {
                    moveSelected(objects, ui.helper);
                },
                stop: function (event, ui) {
                    let item = $(this);
                    item.css('opacity', 1);
                    if (originalObjects && objects) {
                        originalObjects.css('opacity', 1);
                        objects.remove();
                    }
                }
            });
        }

        function findDirectoryChildren(item, type) {
            let objects = filemanagerWindow.find('.directory[data-parent-id="' + item.attr("data-id") + '"]');
            $.each(objects, function () {
                findDirectoryChildren($(this), type);
                if (type === 'hide') {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        }

        function moveSelected(objects, item) {
            let offsetLeft = item.css('left');
            let offsetTop = item.css('top');
            if (objects) {
                objects.each(function () {
                    $this = $(this);
                    $this.css('left', offsetLeft);
                    $this.css('top', offsetTop);
                })
            }
        }

        function move(target, item, route) {
            let data = {
                target: target,
                item: item,
                current: filemanagerWindow.find('.directory.active').attr('data-id')
            };
            if (filemanagerWindow.attr('data-types')) {
                data['types'] = filemanagerWindow.attr('data-types');
            }
            addLoader();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                },
                method: "POST",
                url: route,
                data: data,
                dataType: 'json',
                success: function (response) {
                    directoriesContainer.html(response.directoriesHtml ? response.directoriesHtml : '');
                    filesContainer.html(response.filesHtml ? response.filesHtml : '');
                    removeLoader();
                    initDirectoriesMoving();
                    initFilesMoving();
                },
                error: function (response) {
                    if (response.error) {
                        alert(response.error);
                    } else {
                        alert('Undefined FileManager Error!');
                    }
                }
            });
        }

    </script>
@endsection
