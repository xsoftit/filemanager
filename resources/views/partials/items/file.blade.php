<div class="file-box" data-id="{{$file->id}}" title="{{$file->name}}">
    <div class="file">
        @if($file->preview == 'image')
            <img src="{{$file->path}}">
        @elseif($file->preview == 'video')
            <video>
                <source src="{{$file->path}}" type="video/{{$file->extension}}">
            </video>
        @elseif($file->preview == 'pdf')
            <i class="mdi mdi-file-pdf"></i>
        @elseif($file->preview == 'other')
            <i class="mdi mdi-file-document"></i>
        @endif
    </div>
    <div class="file-shadow">
        <span data-id="{{$file->id}}"
              class="file-info filemanager-button filemanager-button-small filemanager-button-primary"
              title="{{__('filemanager.info')}}"><i class="mdi mdi-information"></i></span>
        <span data-id="{{$file->id}}"
              class="file-download filemanager-button filemanager-button-small filemanager-button-secondary"
              title="{{__('filemanager.download')}}"><i class="mdi mdi-download"></i></span>
        <span data-id="{{$file->id}}"
              class="file-delete filemanager-button filemanager-button-small filemanager-button-red"
              title="{{__('filemanager.delete')}}"><i class="mdi mdi-close-circle"></i></span>
    </div>
    <span class="file-name">{{$file->name}}</span>
</div>
