<div data-id="{{$directory->id}}" class="directory{{$currentDirectory == $directory->id ? ' active' : ''}}{{!$directory->checkTypes($types) ? ' empty' : ''}}" data-parent-id="{{$directory->parent_id}}" data-route="{{$directory->route}}" style="margin-left: {{$directory->margin}}px">
    <span><i class="mdi mdi-folder"></i> {{$directory->name}}</span>
    <i class="directory-delete mdi mdi-close-circle" title="{{__('filemanager.delete')}}"></i>
</div>
@foreach($directory->directories as $dir)
    @include('fileManager::partials.items.directory',['directory' => $dir])
@endforeach
