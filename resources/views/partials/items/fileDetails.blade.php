<div class="filemanager-file-details">
    <div class="preview">
        @if($file->preview == 'image')
            <img src="{{$file->path}}">
        @elseif($file->preview == 'video')
            <video>
                <source src="{{$file->path}}" type="video/{{$file->extension}}">
            </video>
        @elseif($file->preview == 'pdf')
            <i class="mdi mdi-file-pdf"></i>
        @elseif($file->preview == 'other')
            <i class="mdi mdi-file-document"></i>
        @endif
    </div>
    @foreach($file->details as $key => $detail)
        <div class="details-item">
            <label>{{$key}}</label>
            <span>{{$detail}}</span>
        </div>
    @endforeach
</div>
