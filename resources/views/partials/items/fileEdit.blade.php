@if($file->type == 'image')
    <div class="editor">
        <img id="img-edit" src="{{$file->path}}"/>
    </div>
@endif
<div class="data">
    <div class="data-item">
        <label>{{__('filemanager.details.name')}}</label>
        <input name="name" value="{{$file->name}}">
    </div>
    <div class="data-item">
        <label>{{__('filemanager.details.alt')}}</label>
        <input name="alt" value="{{$file->alt}}">
    </div>
</div>
