<div class="filemanager-row">
    <span class="new-files filemanager-button filemanager-button-small filemanager-button-secondary float-right" title="{{__('filemanager.uploading.button')}}"><i class="mdi mdi-upload"></i></span>
</div>
@forelse($files as $file)
    @include('fileManager::partials.items.file',['file' => $file])
@empty
    {{__('filemanager.noFiles')}}
@endforelse
