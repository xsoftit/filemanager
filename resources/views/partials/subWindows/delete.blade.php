<div class="filemanager-delete-window filemanager-sub-window">
    <div class="filemanager-sub-window-box">
        <div class="filemanager-header">
            <h2>{{__('filemanager.deleting.title')}}</h2>
            <button
                class="filemanager-sub-window-close filemanager-button filemanager-button-small filemanager-button-primary float-rigth">
                <i class="mdi mdi-close"></i>
            </button>
        </div>
        <div class="filemanager-body">
            <input type="hidden" class="delete-id">
            {{__('filemanager.deleting.question')}} <b class="delete-object" style="margin-left: 4px"></b>?
        </div>
        <div class="filemanager-footer">
            <span class="submit-delete filemanager-button filemanager-button-red" data-route="" style="margin-left: auto">{{__('filemanager.deleting.submit')}}</span>
            <span class="filemanager-button filemanager-button-primary filemanager-sub-window-close" style="margin-left: 10px">{{__('filemanager.cancel')}}</span>
        </div>
    </div>
</div>
