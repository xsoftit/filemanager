<div class="filemanager-file-info-window filemanager-sub-window">
    <div class="filemanager-sub-window-box">
        <div class="filemanager-header">
            <h2>{{__('filemanager.fileInfo.title')}}</h2>
            <button
                class="filemanager-sub-window-close filemanager-button filemanager-button-small filemanager-button-primary float-rigth">
                <i class="mdi mdi-close"></i>
            </button>
        </div>
        <div class="filemanager-body">

        </div>
        <div class="filemanager-footer">
            <span class="file-edit filemanager-button filemanager-button-secondary" style="margin-left: auto">{{__('filemanager.fileInfo.edit')}}</span>
            <span class="filemanager-button filemanager-button-primary filemanager-sub-window-close" style="margin-left: 10px">{{__('filemanager.cancel')}}</span>
        </div>
    </div>
</div>
