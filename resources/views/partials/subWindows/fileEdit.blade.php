<div class="filemanager-file-edit-window filemanager-sub-window">
    <div class="filemanager-sub-window-box">
        <div class="filemanager-header">
            <h2>{{__('filemanager.fileEdit.title')}}</h2>
            <button
                class="filemanager-sub-window-close filemanager-button filemanager-button-small filemanager-button-primary float-rigth">
                <i class="mdi mdi-close"></i>
            </button>
        </div>
        <div class="filemanager-body">

        </div>
        <div class="filemanager-footer">
            <span class="submit-file-edit filemanager-button filemanager-button-secondary" style="margin-left: auto">{{__('filemanager.fileEdit.save')}}</span>
            <span class="filemanager-button filemanager-button-primary filemanager-sub-window-close" style="margin-left: 10px">{{__('filemanager.cancel')}}</span>
        </div>
    </div>
</div>
