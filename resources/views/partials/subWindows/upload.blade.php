<div class="filemanager-upload-window  filemanager-sub-window">
    <div class="filemanager-sub-window-box">
        <div class="filemanager-header">
            <h2>{{__('filemanager.uploading.title')}}</h2>
            <button
                class="filemanager-sub-window-close filemanager-button filemanager-button-small filemanager-button-primary float-rigth">
                <i class="mdi mdi-close"></i>
            </button>
        </div>
        <div class="filemanager-body">
            <input class="file-upload-input" type="file" multiple>
            <span class="filemanager-upload-info">{{__('filemanager.uploading.info')}}</span>
            <div class="filemanager-uploaded"></div>
        </div>
        <div class="filemanager-footer">
            <span class="upload-files filemanager-button filemanager-button-secondary" style="margin-left: auto">{{__('filemanager.uploading.submit')}}</span>
            <span class="filemanager-button filemanager-button-primary filemanager-sub-window-close" style="margin-left: 10px">{{__('filemanager.cancel')}}</span>
        </div>
    </div>
</div>
