<div class="filemanager-row">
    <span class="new-directory filemanager-button filemanager-button-small filemanager-button-secondary float-right" title="{{__('filemanager.createDirectory')}}"><i class="mdi mdi-folder-plus"></i></span>
</div>
<div class="float-left" style="width: 100%">
@include('fileManager::partials.items.directory',['directory' => $mainDirectory])
</div>
