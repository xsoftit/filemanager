<div class="filemanager-background-shadow"></div>
<div class="filemanager-window">
    <div class="filemanager-content">
        <div class="filemanager-header">
            <h2>{{__('filemanager.title')}}</h2>
            <button
                class="filemanager-close filemanager-button filemanager-button-small filemanager-button-primary float-rigth">
                <i class="mdi mdi-close"></i>
            </button>
        </div>
        <div class="filemanager-body">
            <div class="filemanager-directories">

            </div>
            <div class="filemanager-files">

            </div>
            @include('fileManager::partials.subWindows.upload')
            @include('fileManager::partials.subWindows.fileInfo')
            @include('fileManager::partials.subWindows.fileEdit')
            @include('fileManager::partials.subWindows.delete')
        </div>
        <div class="filemanager-footer">

        </div>
    </div>
</div>

@include('fileManager::js')
